# 阿里云短信

#### 项目介绍
阿里云短信发送sdk

#### 软件架构
~~~
src         模块目录

├─ Alisms        核心目录
        
    ├─ Lib         依赖目录

         ├─ SignatureHelper.php         阿里云短信依赖库。
         
    ├─ Alisms.php            阿里云短信使用类。
~~~

> 本扩展 `1.0.0` 及以上版本，运行环境要求`PHP7.2`以上。


#### 安装教程

1. `码云`   ：git@github.com:china-wangyu/WeChat.git

2. 使用 `composer`  安装
    #### 由于众所周知的原因，国外的网站连接速度很慢。因此安装的时间可能会比较长，我们建议通过下面的方式使用国内镜像。打开命令行窗口（windows用户）或控制台（Linux、Mac 用户）并执行如下命令：
    ~~~
        composer config -g repo.packagist composer https://packagist.phpcomposer.com
    ~~~
    
    #### 使用： 在composer.json添加
    
        "require": {
            "china-wangyu/alisms": "^1.0.0"
        },
    
    #### 然后(命令行)：
    
        composer update

#### 使用说明

1. 加载命名空间
    
        use Alisms\Alisms;
2. 实例化阿里云短信类
        
        /**
         * Alisms constructor.
         * @param string $accessKeyId   阿里云授权ID
         * @param string $accessKeySecret   阿里云授权secret
         * @param string $SignName  阿里云短信签名
         * @param string $TemplateCode  短信模板code
         */
        $alisms = new Alisms($accessKeyId',$accessKeySecret,$SignName,$TemplateCode);
        
3. 发送短信
        
        /**
         * 发送短信
         * @param string $PhoneNumbers  手机号
         * @param array $TemplateParam  短信参数
         * @param string $OutId     流水号设置
         * @return bool|\stdClass   返回结果
         * @throws \Exception
         */
        $res = $alisms->sendSms($PhoneNumbers, $TemplateParam, $OutId);

4. 返回值
    
    成功：
            
            object(stdClass)#149 (4) {
              ["Message"] => string(2) "OK"
              ["RequestId"] => string(36) "EB122598-B375-4644-83E5-D29A6EFA1EA2"
              ["BizId"] => string(20) "385624341141883468^0"
              ["Code"] => string(2) "OK"
            }
    
    失败：
            
            object(stdClass)#149 (3) {
              ["Message"] => string(30) "触发分钟级流控Permits:1"
              ["RequestId"] => string(36) "AE9A8B71-ABE8-4EDC-B10E-2123842EEA33"
              ["Code"] => string(26) "isv.BUSINESS_LIMIT_CONTROL"
            }

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 联系作者

> 注：如有疑问，请联系邮箱 china_wangyu@aliyun.com


> 或，请联系QQ 354007048 / 354937820


<?php
/**
 * Created by china_wangyu (china_wangyu@aliyun.com). Date: 2018/8/2
 */

namespace Alisms;

use Alisms\Lib\SignatureHelper;
class Alisms
{
    private $accessKeyId;
    private $accessKeySecret;
    private $signName;
    private $templateCode;

    /**
     * Alisms constructor.
     * @param string $accessKeyId   阿里云授权ID
     * @param string $accessKeySecret   阿里云授权secret
     * @param string $SignName  阿里云短信签名
     * @param string $TemplateCode  短信模板code
     */
    public function __construct( string $accessKeyId, string $accessKeySecret,string $SignName, string $TemplateCode)
    {
        $this->accessKeyId =$accessKeyId;
        $this->accessKeySecret =$accessKeySecret;
        $this->signName =$SignName;
        $this->templateCode =$TemplateCode;
    }

    /**
     * 发送短信
     * @param string $PhoneNumbers
     * @param array $TemplateParam
     * @param string $OutId
     * @return bool|\stdClass
     * @throws \Exception
     */
    public function sendSms(string $PhoneNumbers, array $TemplateParam = [], string $OutId = 'tiaoba')
    {
        $params = array();
        // fixme 必填: 短信接收号码
        $params["PhoneNumbers"] = $PhoneNumbers;


        // fixme 必填: 短信签名，应严格按"签名名称"填写，请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/sign
        $params["SignName"] = $this->signName;

        // fixme 必填: 短信模板Code，应严格按"模板CODE"填写, 请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/template
        $params["TemplateCode"] = $this->templateCode;

        // fixme 可选: 设置模板参数, 假如模板中存在变量需要替换则为必填项
        $params['TemplateParam'] = $TemplateParam;

        // fixme 可选: 设置发送短信流水号
        $params['OutId'] = "12345";

        // fixme 可选: 上行短信扩展码, 扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段
        $params['SmsUpExtendCode'] = "1234567";

        // *** 需用户填写部分结束, 以下代码若无必要无需更改 ***
        if(!empty($params["TemplateParam"]) && is_array($params["TemplateParam"])) {
            $params["TemplateParam"] = json_encode($params["TemplateParam"], JSON_UNESCAPED_UNICODE);
        }

        try{
            // 初始化SignatureHelper实例用于设置参数，签名以及发送请求
            $helper = new SignatureHelper();

            // 此处可能会抛出异常，注意catch
            $content = $helper->request(
                $this->accessKeyId,
                $this->accessKeySecret,
                "dysmsapi.aliyuncs.com",
                array_merge($params, array(
                    "RegionId" => 'cn-hangzhou',
                    "Action" => "SendSms",
                    "Version" => "2017-05-25",
                ))
            // fixme 选填: 启用https
            // ,true
            );
        }catch (\Exception $exception){
            throw new \Exception($exception->getMessage());
        }
        unset($params);
        return $content;
    }
}